;(function($, window, document, undefined) {
	var $win = $(window);
	var $doc = $(document);

	$doc.ready(function() {
		$('.flexslider').flexslider({
			animation: 'fade',
			controlNav: false, 
			pauseOnAction: false,       
			touch: false,                         
			slideshowSpeed: 3000,     
			directionNav: false
		});		
	});
	
	// $(window).on('load', function() {
	// 	if (isAppleDevice()) {
	// 		url = "https://itunes.apple.com/nl/app/clinck/id706872927?mt=8"
	// 		window.location = url
	// 	}
		
	// 	var isAndroid = navigator.userAgent.toLowerCase().indexOf("android");
	// 	if(isAndroid > -1) {
	// 		url = "https://play.google.com/store/apps/details?id=com.worldsbusinesscard.appOne"
	// 		window.location = url
	// 	}     
	// })
	
	function isAppleDevice(){
		return (
		    (navigator.userAgent.toLowerCase().indexOf("ipad") > -1) ||
		    (navigator.userAgent.toLowerCase().indexOf("iphone") > -1) ||
		    (navigator.userAgent.toLowerCase().indexOf("ipod") > -1)
		);
	}
	

})(jQuery, window, document);
